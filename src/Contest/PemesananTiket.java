package Contest;

class PemesananTiket {
    // Do your magic here...

    private String namaPemesan;
    private TiketKonser tiket;
    private String kodeBooking;
    private String tanggalPesanan;

    public PemesananTiket(String namaPemesan, TiketKonser tiket) {
        this.namaPemesan = namaPemesan;
        this.tiket = tiket;
        this.kodeBooking = Main.generateKodeBooking();
        this.tanggalPesanan = Main.getCurrentDate();
    }

    public void pesanTiket() throws InvalidInputException {
        if (namaPemesan.isEmpty()) {
            throw new InvalidInputException("Nama pemesan tidak boleh kosong.");
        }
    }

    public void tampilkanDetailPemesanan() {
        System.out.println("\n----- Detail Pemesanan -----");
        System.out.println("Nama Pemesan: " + namaPemesan);
        System.out.println("Kode Booking: " + kodeBooking);
        System.out.println("Tanggal Pesanan: " + tanggalPesanan);
        System.out.println("Tiket yang dipesan: " + tiket.getClass().getSimpleName());
        System.out.println("Total harga: " + tiket.getHarga() + " USD");
    }
}
