package Contest;

class CAT1 extends TiketKonser {
    //Do your magic here...

    // Class CAT1 yang merupakan subclass dari TiketKonser
    public CAT1() {
        super("CAT 1");
    }

    public double getHarga() {
        return 2500000;
    }

}